@author:Ahmed Aboukoura
package com.example.andriodproject;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
// this an activity-- which means a screen-- to ad an album, edit an album name, delete an album.
public class AddEditActivity extends AppCompatActivity {

    public static final String ALBUM_INDEX = "albumindex";
    public static final String ALBUM_NAME = "albumname";


    private int albumindex;
    private EditText album_name ;
    private Button delete_button ;
    private TextView addalbums ;
    private TextView editdelete ;
    
    // after the start this method will make the view.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_edit_albums);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // activates the up arrow
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        editdelete = findViewById(R.id.Edit_delete);
        addalbums = findViewById(R.id.AddAlbums);
        delete_button  = findViewById(R.id.button_delete);

        // get the fields
        album_name = findViewById(R.id.album_name);


        // see if info was passed in to populate fields
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            albumindex = bundle.getInt(ALBUM_INDEX);
            album_name.setText(bundle.getString(ALBUM_NAME));
            editdelete.setVisibility(View.VISIBLE);
            delete_button.setVisibility(View.VISIBLE);
            addalbums.setVisibility(View.GONE);
        }else{
            addalbums.setVisibility(View.VISIBLE);
            delete_button.setVisibility(View.GONE);
            editdelete.setVisibility(View.GONE);

        }
    }

    public void cancel(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }
// this method to save an new album.
    public void save(View view) {
        // gather all data from text fields
        String name = album_name.getText().toString();
        if (name.contentEquals("")) {
            Bundle bundle = new Bundle();
            bundle.putString(MovieDialogFragment.MESSAGE_KEY,
                    "Album Name To be saved is required");
            DialogFragment newFragment = new MovieDialogFragment();
            newFragment.setArguments(bundle);
            newFragment.show(getSupportFragmentManager(), "badfields");
            return; // does not quit activity, just returns from method
        }

        // make Bundle
        Bundle bundle = new Bundle();
        bundle.putInt(ALBUM_INDEX, albumindex);
        bundle.putString(ALBUM_NAME,name);


        // send back to caller
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_OK,intent);
        finish(); // pops activity from the call stack, returns to parent

    }
    // this method to delete an album.

    public void delete(View view) {
        // gather all data from text fields
        String name = album_name.getText().toString();
        if (name.contentEquals("") ) {
            Bundle bundle = new Bundle();
            bundle.putString(MovieDialogFragment.MESSAGE_KEY,
                    "Album Name To be Deleted is required");
            DialogFragment newFragment = new MovieDialogFragment();
            newFragment.setArguments(bundle);
            newFragment.show(getSupportFragmentManager(), "badfields");
            return; // does not quit activity, just returns from method
        }

        // make Bundle
        Bundle bundle = new Bundle();
        bundle.putInt(ALBUM_INDEX, albumindex);
        bundle.putString(ALBUM_NAME,name);
        System.out.println("habeby");

        // send back to caller
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_FIRST_USER,intent);
        finish(); // pops activity from the call stack, returns to parent

    }


}