package com.example.andriodproject;



import android.graphics.Bitmap;
import android.net.Uri;

import java.io.Serializable;
import java.net.URI;
import java.time.LocalDate;
import java.util.ArrayList;


public class PictureModel implements Serializable{

    private String caption;
    transient Bitmap pic;

    private ArrayList<Tag> tags;
    private ArrayList<String> tagTypes ;
    private LocalDate date;
    public int AlbumInt;

    public PictureModel() {
        caption = "";
        tags = new ArrayList<Tag>();
        tagTypes = new ArrayList<String>();
        tagTypes.add("Location");
        tagTypes.add("People");
        date = LocalDate.now();


    }

    public String getCaption() {
        return caption;
    }
    public void setAlbumInt(int cool) {
        AlbumInt = cool;
    }
    public int getAlbumInt() {
        return	AlbumInt ;
    }
    public void setTagType(String type) {
        tagTypes.add(type);
    }
    public void setCaption(String cap) {
        caption=cap;
        date = LocalDate.now();
    }
    public Bitmap getImage() {
        return pic;
    }
    public void setImage(Bitmap image) {
        this.pic = image;
    }




    public void addTag(String type, String value) {
        tags.add(new Tag(type, value));
        date = LocalDate.now();
    }


    public void deleteTag(String type, String value) {
        for(Tag t: tags) {
            if(t.getType().contentEquals(type) && t.getValue().contentEquals(value)) {
                tags.remove(t);
            }
        }
        date = LocalDate.now();
    }


    public ArrayList<Tag> getTags() {
        return tags;
    }
    public ArrayList<String> getTagtypes() {
        return tagTypes;
    }
    public void setTags(ArrayList<Tag> t) {
        tags=t;
    }

    public LocalDate getTime() {
        return date;
    }

    public String getTimeString() {
        String editDate = date.toString().substring(5);
        editDate += "-";
        editDate += date.toString().substring(0, 4);
        return editDate;
    }


    public void setDate(LocalDate l) {
        date = l;
    }
}