package com.example.andriodproject;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
/*
 @author:Ahmed Aboukoura.
  
*/
public class SecondActivity extends AppCompatActivity {
    ImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        image = (ImageView) findViewById(R.id.selectedImage);
        Intent intent = getIntent();
        image.setImageResource(intent.getIntExtra("image", 0));
    }
}