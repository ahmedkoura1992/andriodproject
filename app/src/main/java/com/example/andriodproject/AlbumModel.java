package com.example.andriodproject;


import java.io.Serializable;
import java.util.ArrayList;
/*
 @author:Ahmed Aboukoura.
this an album model calss that stores
an array list of picture models and the olderst and the newsest pictures. 
*/
public class AlbumModel implements Serializable{
    public String name;

    private ArrayList<PictureModel> photos;

    private PictureModel oldestPictureModel;
    private PictureModel newestPictureModel;


    public AlbumModel(String n) {
        this.name = n;
        this.photos = new ArrayList<PictureModel>();
    }


    public void oldAndNew() {
        if(photos.isEmpty()) {
            oldestPictureModel=null;
            newestPictureModel=null;
            return;
        }
        oldestPictureModel=photos.get(0);
        newestPictureModel=photos.get(0);;
        for(int i=1; i<photos.size(); i++) {
            if(photos.get(i).getTime().isAfter(newestPictureModel.getTime())) {
                newestPictureModel= photos.get(i);
            }
            if(photos.get(i).getTime().isBefore(oldestPictureModel.getTime())) {
                oldestPictureModel= photos.get(i);
            }
        }

    }

    public void setName(String str) {
        name = str;
    }


    public void addPictureModel(PictureModel pic) {
        photos.add(pic);
    }


    public ArrayList<PictureModel> getPictureModels() {
        return photos;
    }


    public PictureModel getOldestPictureModel() {
        return oldestPictureModel;
    }


    public PictureModel getNewestPictureModel() {
        return newestPictureModel;
    }


    public void deletePictureModel(PictureModel pic) {
        photos.remove(pic);
    }

    @Override
    public String toString() {
        return name;
    }
    public String getString() {
        return name;
    }

}