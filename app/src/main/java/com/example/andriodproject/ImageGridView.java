package com.example.andriodproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
/*
 @author:Ahmed Aboukoura.
  this class for the grid view of the images from albums. 
*/
public class ImageGridView extends AppCompatActivity {
    public static final String ALBUM_INDEX = "albumindex";
    public static final String ALBUM_NAME = "albumname";
    int albumindex ;
    public String albumname;
     public ImageAdapter myImgAdapter;
   public  Bitmap bitmp;
    public ArrayList<PictureModel> pictureModels;
   public  GridView gridview;
    public ArrayList<AlbumModel> AlbumModels;
    public static final int EDIT_MOVIE_CODE = 1;
    public static final int ADD_MOVIE_CODE = 2;
   // Bitmap logos[] ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
          pictureModels = new ArrayList<PictureModel>();
/////////////////////////////
        try {
            FileInputStream fiss = openFileInput("photoalbums.dat");
            ObjectInputStream oiss = new ObjectInputStream(fiss);
            AlbumModels = (ArrayList<AlbumModel>) oiss.readObject();

            System.out.println(AlbumModels.get(albumindex).getPictureModels().size() + "fffffffffffffffff");
            fiss.close();
            oiss.close();
        } catch (FileNotFoundException e) {
            //  return ;
        } catch (IOException e) {
            //  return ;
        } catch (ClassNotFoundException e) {
            //  return ;
        } catch (Exception e) {
            //return ;
        }
      ////////////////////
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            albumindex = bundle.getInt(ALBUM_INDEX);
            albumname = bundle.getString(ALBUM_NAME);
        }
        /////////////
        if(AlbumModels.get(albumindex).getPictureModels() != null && !AlbumModels.get(albumindex).getPictureModels().isEmpty()){
           int size = AlbumModels.get(albumindex).getPictureModels().size();
         System.out.println(size+"cccccccccccccc");
         System.out.println(AlbumModels.get(albumindex).getPictureModels().get(0).getImage());
           for(int i = 0;i<size;i++){
               pictureModels.add(AlbumModels.get(albumindex).getPictureModels().get(i));

            }
        }
        System.out.println(pictureModels.size()+"lololollo");

        //////////
        setContentView(R.layout.grid_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        /////
        if (pictureModels != null) {

            // activates the up arrow
          System.out.println("fgfgfgfgfgfgfgfgfgfg");
          if(pictureModels != null && !pictureModels.isEmpty() ) {
              ImageAdapter myImgAdapter = new ImageAdapter(this, pictureModels);
              gridview = findViewById(R.id.gridview);
              gridview.setAdapter(myImgAdapter);
          }

        }
    }
    ///////////
    public boolean onCreateOptionsMenu(Menu menu2) {
        getMenuInflater().inflate(R.menu.add_menu2, menu2);
        return super.onCreateOptionsMenu(menu2);
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add2:
                addpicture();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void addpicture() {
        Intent intent = new Intent(this, AddEditphoto.class);
        startActivityForResult(intent, ADD_MOVIE_CODE);
    }

    protected void onActivityResult(int requestCode,
                                    int resultCode,
                                    Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        ArrayList<AlbumModel> AlbumModelss = new ArrayList<AlbumModel>() ;
        ArrayList<PictureModel >pictureModelss = new ArrayList<PictureModel>();
        if (resultCode == RESULT_CANCELED) {
            return;
        }
/////////////////////////////
        try {
            ArrayList<AlbumModel> tempAlbumModels = new ArrayList<AlbumModel>();
            FileInputStream fiss = openFileInput("photoalbums.dat");
            ObjectInputStream oiss = new ObjectInputStream(fiss);
            tempAlbumModels = (ArrayList<AlbumModel>) oiss.readObject();

            System.out.println(tempAlbumModels.get(albumindex).getPictureModels().size() + "5555555555555");
            AlbumModelss = tempAlbumModels;
            fiss.close();
            oiss.close();
        } catch (FileNotFoundException e) {
            //  return ;
        } catch (IOException e) {
            //  return ;
        } catch (ClassNotFoundException e) {
            //  return ;
        } catch (Exception e) {
            //return ;
        }
        ////////////////////
        Bundle bundle = intent.getExtras();
        if (bundle == null) {
            return;
        } if(resultCode == RESULT_OK) {
          //  ArrayList<AlbumModel> temp2lbumModels = new ArrayList<AlbumModel>();
            System.out.println("--------------------------");
            byte[] bitmapdata= bundle.getByteArray("image");

            Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapdata , 0, bitmapdata .length);

             System.out.println(bitmap + "--------------------------");


            PictureModel temp = new PictureModel();
            temp.setImage(bitmap);
            System.out.println(albumindex +"yyyyyyyyyyyyyyxxxxxxxxxxyyyyyyyyyyyyy"+temp.toString());
            AlbumModelss.get(albumindex).addPictureModel(temp);
            System.out.println(AlbumModelss.get(albumindex).getPictureModels().size() + "5555555555555");
            System.out.println(AlbumModelss.get(albumindex).getNewestPictureModel() +"yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");

            System.out.println("************");
            System.out.println(AlbumModelss.toString());
//            System.out.println(AlbumModelss.get(albumindex).getPictureModels().get(0).getImage().toString());
            }

        ObjectOutputStream oos;
        try {
            FileOutputStream fileOutputStream = openFileOutput("photoalbums.dat", Context.MODE_PRIVATE);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(AlbumModelss);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(AlbumModelss);
        System.out.println("<<<<<<<<<<<");
            if(AlbumModelss != null && !AlbumModelss.isEmpty()) {
                //////////////////
                ArrayList<PictureModel >pictureModelsks = new ArrayList<PictureModel>();
                if(AlbumModelss.get(albumindex).getPictureModels() != null && !AlbumModelss.get(albumindex).getPictureModels().isEmpty()){
                    int size = AlbumModelss.get(albumindex).getPictureModels().size();
                    System.out.println(size+"cccccccccccccc");
//           System.out.println(AlbumModels.get(albumindex).getPictureModels().get(2).getImage().toString());
                    for(int i = 0;i<size;i++){
                        pictureModelsks.add(AlbumModelss.get(albumindex).getPictureModels().get(i));
                        pictureModelss = pictureModelsks;

                    }
                }
                ///////////////
                System.out.println(">>>>>>>>>>>>>>>");
                System.out.println(AlbumModelss.toString()+">>>>>>>>>>>>>>>");
                 myImgAdapter = new ImageAdapter(this, pictureModelss);
                System.out.println(pictureModelss.size()+"9999999999");
                System.out.println(pictureModelss.get(0).getImage());
                System.out.println(myImgAdapter.toString());
                gridview.setAdapter(myImgAdapter);
            }
        }

    }

