package com.example.andriodproject;


import java.io.Serializable;

// this is a class, to store the photo tags with the name and type as a string. 
public class Tag implements Serializable{

    private String type, value;

    public Tag(String t, String v) {
        this.type = t;
        this.value = v;
    }


    public String getType() {
        return type;
    }


    public void setType(String t) {
        this.type = t;
    }


    public String getValue() {
        return value;
    }



    public void setValue(String v) {
        this.value = v;
    }

    @Override
    public String toString() {
        return type + ": " + value;
    }


    public boolean equals(Tag t) {
        if(value.contentEquals(t.getValue()) && type.contentEquals(t.getType())) {
            return true;
        }
        return false;
    }

}