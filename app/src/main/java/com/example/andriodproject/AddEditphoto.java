@author:Ahmed Aboukoura.
package com.example.andriodproject;

import android.app.Notification;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
/*
 @author:Ahmed Aboukoura.
 this class to show the photos in an album then allow the user to add, edit,
 remove a photos from an album. 
*/
public class AddEditphoto extends AppCompatActivity {





    private int albumindex;
    private EditText photo_name ;
    private Button delete_button ;
    private TextView addphotos ;
    public Bitmap ImageBitmap;
    TextView textTargetUri;
    ImageView targetImage;

    private TextView editdelete ;

    @Override
    // this on create, it will show the photos. 
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_edit_photos);
        Button buttonLoadImage = (Button)findViewById(R.id.loadimage);
        photo_name = findViewById(R.id.photo_name);
        Toolbar toolbar = findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);

        // activates the up arrow
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        buttonLoadImage.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 0);
            }});
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK){
            Uri ImageUri = data.getData();
            if(ImageUri != null) {
               photo_name.setText("Photo Loaded successfuly!");
            }
            ImageView iv = new ImageView(this);

            iv.setImageURI(ImageUri);

            BitmapDrawable drawable = (BitmapDrawable) iv.getDrawable();
             ImageBitmap = drawable.getBitmap();
            System.out.println(ImageBitmap.toString()+"kkkkkkkkkkk");
        }
    }
    public void cancel(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }
    // Save photos to the album
    public void save(View view) {
        // gather all data from text fields
        String temp = photo_name.getText().toString();
        if (temp.contentEquals("")) {
            Bundle bundle = new Bundle();
            bundle.putString(MovieDialogFragment.MESSAGE_KEY,
                    "Album Name To be saved is required");
            DialogFragment newFragment = new MovieDialogFragment();
            newFragment.setArguments(bundle);
            newFragment.show(getSupportFragmentManager(), "badfields");
            return; 
            // does not quit activity, just returns from method
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        System.out.println(ImageBitmap.toString()+"wwwwwwwwwwwwww");
        System.out.println(stream+"wwwggggggggwwwwwwwwwww");
        ImageBitmap.compress(Bitmap.CompressFormat.PNG, 20, stream);
        byte[] byteArray = stream.toByteArray();
         System.out.println(byteArray.toString()+"pppppppppppppppppppppppppppppppppp");
        Bundle bundle = new Bundle();
        bundle.putByteArray("image",byteArray);


        // send back to caller
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_OK,intent);
        finish(); // pops activity from the call stack, returns to parent

    }


}