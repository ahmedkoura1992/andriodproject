package com.example.andriodproject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
/*
 @author:Ahmed Aboukoura.
 this the first activity which will show all the albums that have been saved from previous sessions.
*/
public class FirstActivity extends AppCompatActivity {

    public ListView listView;
    private ArrayList<AlbumModel> AlbumModels;

    public static final int EDIT_MOVIE_CODE = 1;
    public static final int ADD_MOVIE_CODE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        AlbumModels = new ArrayList<AlbumModel>();
       // AlbumModels.add(new AlbumModel("Test-Album Feel Free To delete"));

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);



        try {
            FileInputStream fis = openFileInput("photoalbums.dat");
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(fis));
            if(br.readLine() == null){
                fis.close();
                AlbumModels = new ArrayList<AlbumModel>();
                //AlbumModels.add(new AlbumModel("Test-Album Feel Free To delete"));
                ObjectOutputStream oos;
                try {
                    FileOutputStream fileOutputStream = openFileOutput("photoalbums.dat", Context.MODE_PRIVATE);
                    ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
                    objectOutputStream.writeObject(AlbumModels);
                    objectOutputStream.close();
                    fileOutputStream.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();

                } catch (IOException e) {
                    e.printStackTrace();
                }finally {
                }

            }else{


                try {
                    FileInputStream fiss = openFileInput("photoalbums.dat");
                    ObjectInputStream oiss = new ObjectInputStream(fiss);
                    AlbumModels = (ArrayList<AlbumModel>) oiss.readObject();
                    fiss.close();
                    oiss.close();
                } catch (FileNotFoundException e) {
                    return ;
                } catch (IOException e) {
                    return ;
                } catch (ClassNotFoundException e) {
                    return ;
                } catch (Exception e) {
                    return ;
                }
            }


        } catch (FileNotFoundException e) {
            return ;
        } catch (IOException e) {
            return ;
        }  catch (Exception e) {
            return ;
        }

        listView = findViewById(R.id.movies_list);
        listView.setLongClickable(true);
        listView.setAdapter(
                new ArrayAdapter<AlbumModel>(this, R.layout.content_first, AlbumModels));

        // show movie for possible edit when tapped
        listView.setOnItemLongClickListener((p, V, pos, id) ->
                showMovie(pos));
        listView.setOnItemClickListener((p, V, pos, id) ->
                showAlbum(pos));
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }



    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                addMovie();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addMovie() {
        Intent intent = new Intent(this, AddEditActivity.class);
        startActivityForResult(intent, ADD_MOVIE_CODE);
    }



    private boolean showMovie(int pos) {
        Bundle bundle = new Bundle();
        AlbumModel albumModel = AlbumModels.get(pos);
        bundle.putInt(AddEditActivity.ALBUM_INDEX, pos);
        bundle.putString(AddEditActivity.ALBUM_NAME, albumModel.getString());

        Intent intent = new Intent(this, AddEditActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, EDIT_MOVIE_CODE);
        return true;
    }
    private boolean showAlbum(int pos) {
        Bundle bundle = new Bundle();
        AlbumModel albumModel = AlbumModels.get(pos);
        bundle.putInt(AddEditActivity.ALBUM_INDEX, pos);
        bundle.putString(AddEditActivity.ALBUM_NAME, albumModel.getString());

        Intent intent = new Intent(this, ImageGridView.class);

        intent.putExtras(bundle);
        startActivity(intent);
        return true;
    }



    protected void onActivityResult(int requestCode,
                                    int resultCode,
                                    Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode == RESULT_CANCELED) {
            return;
        }

        Bundle bundle = intent.getExtras();
        if (bundle == null) {
            return;
        }

        // gather all info passed back by launched activity
        String name = bundle.getString(AddEditActivity.ALBUM_NAME);
        int index = bundle.getInt(AddEditActivity.ALBUM_INDEX);

        if (requestCode == EDIT_MOVIE_CODE) {
            if(resultCode == RESULT_FIRST_USER){
                AlbumModels.remove(index)  ;
                ObjectOutputStream oos;
                try {
                    FileOutputStream fileOutputStream = openFileOutput("photoalbums.dat", Context.MODE_PRIVATE);
                    ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
                    objectOutputStream.writeObject(AlbumModels);
                    objectOutputStream.close();
                    fileOutputStream.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else {
                AlbumModel albumModel = AlbumModels.get(index);
                albumModel.name = name;
            }

        } else {

            if(resultCode == RESULT_FIRST_USER) {
                int Indextemp = -1;
                for(int i =0;i<AlbumModels.size();i++){
                    AlbumModel temp = AlbumModels.get(i);
                    if(temp.name.contentEquals(name)){
                        Indextemp = i;

                    }
                }if(Indextemp == -1){
                    return;
                }
                AlbumModels.remove(Indextemp);

            }else if(resultCode == RESULT_OK) {
                AlbumModels.add(new AlbumModel(name));
            }
            ObjectOutputStream oos;
            try {
                FileOutputStream fileOutputStream = openFileOutput("photoalbums.dat", Context.MODE_PRIVATE);
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
                objectOutputStream.writeObject(AlbumModels);
                objectOutputStream.close();
                fileOutputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }





        // redo the adapter to reflect change^K
        listView.setAdapter(
                new ArrayAdapter<AlbumModel>(this, R.layout.content_first, AlbumModels));
    }

}

