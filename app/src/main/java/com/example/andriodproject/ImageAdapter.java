package com.example.andriodproject;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;
/*
 @author:Ahmed Aboukoura.
   This an imageadapter, refere to the andriod studio documentation for more. 
*/
public class ImageAdapter extends BaseAdapter {
    Context context;
    ArrayList<PictureModel> pictureModels;

    LayoutInflater inflter;
    public ImageAdapter(Context applicationContext, ArrayList<PictureModel> pictureModels) {
        this.context = applicationContext;
        this.pictureModels = pictureModels;
        inflter = (LayoutInflater.from(applicationContext));
    }
    @Override
    public int getCount() {
        return pictureModels.size();
    }
    @Override
    public Object getItem(int i) {
        return null;
    }
    @Override
    public long getItemId(int i) {
        return 0;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if(view == null){
           LayoutInflater l = LayoutInflater.from(context);
            view = inflter.inflate(R.layout.toast_layout, null);
        }
         // inflate the layout
        ImageView koko = (ImageView) view.findViewById(R.id.image); // get the reference of ImageView

        koko.setImageBitmap(pictureModels.get(i).getImage()); // set logo images
        System.out.println(pictureModels.size()+"EEEEEEEEEEEEE");
        System.out.println(pictureModels.get(i).getImage()+"bbbbbbbbbbbb");
        return view;
    }
}